---
sidebar_position: 1
---

# Introducción

Este manual es una, guia trasladada de mi antiguo blog personal. Es un manual guiado paso a paso, para montar en una máquina Linux, diferentes tipos de servidores bajo unos criterios. No se entra en detalle en muchas cosas, pero se explican otras muchas que a día de hoy me sigue resultando útil cuando tengo alguna duda sobre algo. Hace poco tuve que mirar como cambiar las interfaces de red a su antiguo nombre, y en este manual lo tenía comentado. Son estas pequeñas cosas por las que a veces merece la pena tener guardado un documento aunque sea simple.

